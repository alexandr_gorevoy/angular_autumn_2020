import {ActionReducerMap} from '@ngrx/store';
import * as auth from './reducers/auth.reducer';
import {authFeatureKey} from './reducers/auth.reducer';
import {coursesFeatureKey, coursesReducer, CoursesState} from './reducers/courses.reducer';
import {courseModifyedFeatureKey, courseModifyedReducer, CourseModifyedState} from './reducers/course-modifyed.reducer';


export interface State {
  [authFeatureKey]: auth.AuthState;
  [coursesFeatureKey]: CoursesState;
  [courseModifyedFeatureKey]: CourseModifyedState;
}

export const reducers: ActionReducerMap<State> = {
  [authFeatureKey]: auth.reducer,
  [coursesFeatureKey]: coursesReducer,
  [courseModifyedFeatureKey]: courseModifyedReducer
};
