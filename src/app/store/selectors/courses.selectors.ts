import {createFeatureSelector, createSelector} from '@ngrx/store';
import {coursesFeatureKey, CoursesState} from '../reducers/courses.reducer';

export const selectCoursesFeature = createFeatureSelector<CoursesState>(coursesFeatureKey);

export const selectCourses = createSelector(
  selectCoursesFeature,
  (state: CoursesState) => state
);
