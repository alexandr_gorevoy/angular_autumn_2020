import {createFeatureSelector, createSelector} from '@ngrx/store';
import {authFeatureKey, AuthState} from '../reducers/auth.reducer';

export const selectAuthFeature = createFeatureSelector<AuthState>(authFeatureKey);

export const selectAuth = createSelector(
  selectAuthFeature,
  (state: AuthState) => state
);
