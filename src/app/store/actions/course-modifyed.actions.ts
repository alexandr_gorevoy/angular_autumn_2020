import {createAction} from '@ngrx/store';

export enum CourseModifyedActions {
  Added = '[CourseModifyed] New Added',
  Deleted = '[CourseModifyed] Deleted',
}

export const courseAdded = createAction(
  CourseModifyedActions.Added
);


export const courseDeleted = createAction(
  CourseModifyedActions.Deleted
);
