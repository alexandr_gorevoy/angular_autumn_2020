import {createAction, props} from '@ngrx/store';
import {CoursesState} from '../reducers/courses.reducer';

export enum CoursesActions {
  InProgress = '[Courses] In Progress',
  Loaded = '[Courses] Loaded',
  Error = '[Courses] Error'
}

export const loadingCoursesInProgress = createAction(
  CoursesActions.InProgress
);

export const loadCoursesSuccess = createAction(
  CoursesActions.Loaded,
  props<CoursesState>()
);

export const loadCoursesFailure = createAction(
  CoursesActions.Error
);


