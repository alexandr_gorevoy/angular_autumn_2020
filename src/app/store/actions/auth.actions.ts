import {createAction, props} from '@ngrx/store';
import {AuthState} from '../reducers/auth.reducer';

export enum AuthActionTypes {
  LoadAuths = '[Auth] In progress',
  LoadAuthsSuccess = '[Auth] Logged in',
  LoadAuthsFailure = '[Auth] Logged in error',
  LoadAuthsLoggedOff = '[Auth] Logged off',
}

export const loadAuths = createAction(
  AuthActionTypes.LoadAuths
);

export const loadAuthsSuccess = createAction(
  AuthActionTypes.LoadAuthsSuccess,
  props<AuthState>()
);

export const loadAuthsFailure = createAction(
  AuthActionTypes.LoadAuthsFailure,
  props<AuthState>()
);

export const loadAuthsLoggedOff = createAction(
  AuthActionTypes.LoadAuthsLoggedOff
);

