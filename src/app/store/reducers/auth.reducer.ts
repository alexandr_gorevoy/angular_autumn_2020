import {Action, createReducer, on} from '@ngrx/store';
import {User} from '../../core/models/user.model';
import * as AuthActions from '../actions/auth.actions';

export const authFeatureKey = 'auth';

export interface AuthState {
  isAuthenticated: boolean;
  user: User | null;
  errorMessage: string | null;
  token?: number | null;
}

export const initialState: AuthState = {
  isAuthenticated: false,
  user: null,
  errorMessage: null,
  token: null
};


const authReducer = createReducer(
  initialState,
  on(AuthActions.loadAuths, state => ({...state})),
  on(AuthActions.loadAuthsSuccess, (state, authData) =>
    ({...state,
      isAuthenticated: authData.isAuthenticated,
      user: authData.user,
      token: authData.token,
      errorMessage: null
    })),
  on(AuthActions.loadAuthsFailure, (state, authData) =>
    ({...state,
      errorMessage: authData.errorMessage
    })),
  on(AuthActions.loadAuthsLoggedOff, state => ({...state, ...initialState})),
);

export function reducer(state: AuthState | undefined, action: Action): AuthState {
  return authReducer(state, action);
}

