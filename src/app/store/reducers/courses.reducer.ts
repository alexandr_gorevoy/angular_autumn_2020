import {Action, createReducer, on} from '@ngrx/store';
import * as CoursesActions from '../actions/courses.actions';
import {Course} from '../../core/models/course.model';


export const coursesFeatureKey = 'courses';


export interface CoursesState {
  coursesArr: Course[];
}

export const initialState: CoursesState = {
  coursesArr: null
  };



export const coursesReducer = createReducer(
  initialState,
  on(CoursesActions.loadingCoursesInProgress, state => ({...state})),
  on(CoursesActions.loadCoursesSuccess, (state, courses) => ({
    ...state,
    ...courses
  })),
  on(CoursesActions.loadCoursesFailure, state => ({...state})),
);

export function reducer(state: CoursesState | undefined, action: Action): CoursesState {
  return coursesReducer(state, action);
}
