import {Action, createReducer, on} from '@ngrx/store';
import * as CourseModifyedActions from '../actions/course-modifyed.actions';
import {Course} from '../../core/models/course.model';


export const courseModifyedFeatureKey = 'courseModifyed';

export interface CourseModifyedState {
  course: Course;
}

export const initialState: CourseModifyedState = {
  course: null
};


export const courseModifyedReducer = createReducer(
  initialState,
  on(CourseModifyedActions.courseAdded, state => ({...state})),
  on(CourseModifyedActions.courseDeleted, state => ({...state})),
);

export function reducer(state: CourseModifyedState | undefined, action: Action): CourseModifyedState {
  return courseModifyedReducer(state, action);
}

