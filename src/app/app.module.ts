import {BrowserModule} from '@angular/platform-browser';
import {NgModule, Provider} from '@angular/core';
import {AlertModule} from 'ngx-bootstrap/alert';

import {AppComponent} from './app.component';
import {CoursePageComponent} from './components/course-page/course-page.component';
import {BreadcrumbsComponent} from './components/breadcrumbs/breadcrumbs.component';
import {PageFooterComponent} from './components/page-footer/page-footer.component';
import {CourseComponent} from './components/course/course.component';
import {SearchSectionComponent} from './components/search-section/search-section.component';
import {CoursesListComponent} from './components/courses-list/courses-list.component';
import {PageHeaderComponent} from './components/page-header/page-header.component';
import {FormsModule} from '@angular/forms';
import {HightlightBorderDirective} from './core/directives/hightlight-border.directive';
import {DurationPipe} from './core/pipes/duration.pipe';
import {AddCourseComponent} from './components/add-course/add-course.component';
import {AddCourseDateComponent} from './components/add-course/add-course-date/add-course-date.component';
import {AddCourseDurationComponent} from './components/add-course/add-course-duration/add-course-duration.component';
import {AddCourseAuthorsComponent} from './components/add-course/add-course-authors/add-course-authors.component';
import {AppRoutingModule} from './app-routing.module';
import {LoginPageModule} from './components/login-page/login-page.module';
import {ErrorPageComponent} from './components/error-page/error-page.component';
import {CoursePageContainerComponent} from './components/course-page/course-page-container/course-page-container.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {PaginationComponent} from './components/pagination/pagination.component';
import {AuthInterceptor} from './core/interceptors/auth.interceptor';
import {LoaderComponent} from './components/loader/loader.component';
import {StoreModule} from '@ngrx/store';
import {reducers} from './store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';

const INTERCEPTOR: Provider = {
  provide: HTTP_INTERCEPTORS,
  useClass: AuthInterceptor,
  multi: true
};

@NgModule({
  declarations: [
    AppComponent,
    CoursePageComponent,
    PageHeaderComponent,
    BreadcrumbsComponent,
    PageFooterComponent,
    CoursesListComponent,
    CourseComponent,
    SearchSectionComponent,
    HightlightBorderDirective,
    DurationPipe,
    AddCourseComponent,
    AddCourseDateComponent,
    AddCourseDurationComponent,
    AddCourseAuthorsComponent,
    ErrorPageComponent,
    CoursePageContainerComponent,
    BreadcrumbsComponent,
    PaginationComponent,
    LoaderComponent,
  ],
  imports: [
    BrowserModule,
    AlertModule.forRoot(),
    FormsModule,
    AppRoutingModule,
    LoginPageModule,
    HttpClientModule,
    StoreModule.forRoot(reducers, {
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),
    !environment.production ?
      StoreDevtoolsModule.instrument({
        maxAge: 25,
        logOnly: environment.production
      }) :
      [],
  ],
  providers: [INTERCEPTOR],
  bootstrap: [AppComponent]
})
export class AppModule { }
