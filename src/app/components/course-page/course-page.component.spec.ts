// import { ComponentFixture, TestBed } from '@angular/core/testing';
//
// import { CoursePageComponent } from './course-page.component';
// import {By} from '@angular/platform-browser';
// import {Course} from '../../core/models/course.model';
// import {FilterPipe} from '../../core/pipes/filter.pipe';
// import {OrderByPipe} from '../../core/pipes/order-by.pipe';
//
// const SELECTORS = {
//   deleteBtn: `[data-testId="deleteBtn"]`
// };
//
// describe('CoursePageComponent', () => {
//   let component: CoursePageComponent;
//   let fixture: ComponentFixture<CoursePageComponent>;
//   const millisecondsInDay = 86400000;
//
//   beforeEach(async () => {
//     await TestBed.configureTestingModule({
//       declarations: [ CoursePageComponent ],
//       providers: [FilterPipe, OrderByPipe]
//     })
//     .compileComponents();
//   });
//
//   beforeEach(() => {
//     fixture = TestBed.createComponent(CoursePageComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });
//
//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
//
//   it('should call loadMoreCourses once read-more__btn clicked', () => {
//     spyOn(component, 'loadMoreCourses');
//
//     fixture.debugElement.query(By.css(SELECTORS.deleteBtn)).triggerEventHandler('click', null);
//     fixture.detectChanges();
//
//     expect(component.loadMoreCourses).toHaveBeenCalled();
//   });
//
//   it('should call console.log', () => {
//     spyOn(console, 'log');
//
//     component.loadMoreCourses();
//
//     expect(console.log).toHaveBeenCalled();
//     });
//
//   it('should return filtered courses', () => {
//     const filteredCourse: Course[] = [
//       {
//         id: 1,
//         creationDate: Date.now() + millisecondsInDay,
//         duration: 128,
//         description: 'fog',
//         title: 'In the Future We Will All Live in Space',
//         topRated: false
//       }
//     ];
//
//     spyOn(component.filterPipe, 'transform').and.returnValue(filteredCourse);
//
//     component.filterCourses('test');
//
//     expect(component.filteredCourses).toEqual(filteredCourse);
//   });
// });
