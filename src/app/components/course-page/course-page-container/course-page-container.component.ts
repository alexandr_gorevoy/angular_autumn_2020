import {Component, OnInit} from '@angular/core';
import {CoursesService} from '../../../core/services/courses.service';
import {Course} from '../../../core/models/course.model';
import {PaginationService} from '../../../core/services/pagination.service';
import {HttpService} from '../../../core/services/http.service';
import {select, Store} from '@ngrx/store';
import {loadCoursesFailure, loadCoursesSuccess} from '../../../store/actions/courses.actions';
import {Subscription} from 'rxjs';
import {selectCourses} from '../../../store/selectors/courses.selectors';

@Component({
  selector: 'app-course-page',
  templateUrl: './course-page-container.component.html',
  styleUrls: ['./course-page-container.component.scss']
})

export class CoursePageContainerComponent implements OnInit {
  filteredCourses: Course[];

  constructor(private coursesService: CoursesService,
              private paginationService: PaginationService,
              private http: HttpService,
              private store: Store) {
  }

  isCoursesEmpty(): boolean {
    return !!this.coursesService.coursesFromServer?.length;
  }

  ngOnInit(): void {
    this.coursesService.getCoursesList().subscribe({
      next: (courses) => {
        this.store.dispatch(loadCoursesSuccess({coursesArr: courses}));
        this.setCoursesFromStore();

        this.paginationService.setPagesAmount(this.filteredCourses);
        this.paginate(1);
      },
      error: (err) => {
        this.store.dispatch(loadCoursesFailure());
        console.error(err);
      }
    });
  }

  loadMoreCourses(): void {
    console.log('Load more');
  }

  filterCourses(searchFieldText: string): void {
    this.http.getFilteredCoursesList(searchFieldText).subscribe(courses => {
      this.filteredCourses = courses;
    });
  }

  paginate(pageNumber: number): void {
    this.filteredCourses = this.paginationService.getCoursesforSpecificPage(pageNumber, this.coursesService.coursesFromServer);
  }

  setCoursesFromStore(): Subscription {
    return this.store.pipe(select(selectCourses)).subscribe(
      coursesStore => {
        this.filteredCourses = this.coursesService.coursesFromServer = [...coursesStore.coursesArr];
      }
    );
  }
}
