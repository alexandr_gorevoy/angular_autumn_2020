import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesListComponent } from './courses-list.component';
import {Component, OnInit} from '@angular/core';
import {Course} from '../../core/models/course.model';

describe('CoursesListComponent', () => {
  let component: CoursesListComponent;
  let fixture: ComponentFixture<CoursesListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CoursesListComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call console.log with proper id', () => {
    spyOn(console, 'log');

    component.deleteCourse(5);

    expect(console.log).toHaveBeenCalledWith(5);
  });
});

  // Test host

const SELECTORS = {
  coursesList: `[data-testId="coursesList"]`
};

describe('CoursesListComponent inside test host', () => {
  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let coursesList;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CoursesListComponent, TestHostComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;
    coursesList = fixture.nativeElement.querySelector(SELECTORS.coursesList);

    fixture.detectChanges();
  });

  it('should render proper amount of courses, taken from host component' , () => {
    expect(coursesList.childElementCount).toEqual(2);
  });

  it('should not render courses if no data provided' , () => {
    component.courses = [];

    fixture.detectChanges();

    expect(coursesList.childElementCount).toEqual(0);
  });
});

  // test host component

@Component({
  template: `
    <app-courses-list data-testId="coursesList" [coursesArr]="courses"></app-courses-list>
  `
  })

class TestHostComponent implements  OnInit{
  courses: Course[];

  ngOnInit(): void {
    this.courses = [
      {
        id: 55,
        date: Date.parse('Sept 02, 20'),
        length: 12,
        description: `Fog`,
        name: 'I',
        isTopRated: true
      },
      {
        id: 55,
        date: Date.parse('Sept 02, 20'),
        length: 12,
        description: `Fog`,
        name: 'I',
        isTopRated: false
      },
    ];
  }
}

