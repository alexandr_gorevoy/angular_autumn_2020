import {Component, Input} from '@angular/core';
import {Course} from '../../core/models/course.model';
import {CoursesService} from '../../core/services/courses.service';
import {PaginationService} from '../../core/services/pagination.service';
import {concatMap} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import {courseDeleted} from '../../store/actions/course-modifyed.actions';

@Component({
  selector: 'app-courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.scss']
})
export class CoursesListComponent {
  @Input() coursesArr: Course[];


  constructor(private coursesService: CoursesService,
              private paginationService: PaginationService,
              private store: Store) {
  }

  deleteCourse(id: number): void {
    if (confirm('Do you really want to delete this course?')) {
      this.coursesService.removeItem(id)
        .pipe(
          concatMap(req => this.coursesService.getCoursesList())
        ).subscribe( (courses) => {
        this.store.dispatch(courseDeleted());
        this.coursesArr = this.paginationService.getCoursesforSpecificPage(this.paginationService.currentPage, courses);
        });
    }
  }
}
