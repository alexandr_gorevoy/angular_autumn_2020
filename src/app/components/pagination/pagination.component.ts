import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Course} from '../../core/models/course.model';
import {CoursesService} from '../../core/services/courses.service';
import {PaginationService} from '../../core/services/pagination.service';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  @Output() listOfPaginatedCoursesEmit: EventEmitter<number> = new EventEmitter<number>();

  constructor(private courseService: CoursesService,
              private paginationService: PaginationService) { }

  pagesAmount;

  ngOnInit(): void {
    this.pagesAmount = this.paginationService.pagesAmount;
  }

  onPageClick(currentPage): void {
    const nextPage = ++currentPage;
    this.paginationService.currentPage = nextPage;
    this.listOfPaginatedCoursesEmit.emit(nextPage);
  }
}
