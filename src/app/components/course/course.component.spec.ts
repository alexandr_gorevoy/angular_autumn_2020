import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseComponent } from './course.component';
import {By} from '@angular/platform-browser';
import {Component, DebugElement} from '@angular/core';
import {Course} from '../../core/models/course.model';
import {DurationPipe} from '../../core/pipes/duration.pipe';
import {DatePipe} from '@angular/common';

const SELECTORS = {
  courseDuration:  `[data-testId="courseDuration"]`,
  courseCreationDate: `[data-testId="courseCreationDate"]`,
  courseTitle: `[data-testId="courseTitle"]`,
  courseDescription: `[data-testId="courseDescription"]`,
  deleteBtn: `[data-testId="deleteBtn"]`
};

describe('CourseComponent', () => {
  let component: CourseComponent;
  let fixture: ComponentFixture<CourseComponent>;
  let deleteBtn: DebugElement;
  const mockCourse = {
    duration: 123,
    title: 'title',
    id: 15,
    description: 'desk',
    creationDate: Date.parse('Sept 02, 20'),
    topRated: false
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CourseComponent, DurationPipe ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseComponent);
    component = fixture.componentInstance;
    // component.course = mockCourse;
    deleteBtn = fixture.debugElement.query(By.css(SELECTORS.deleteBtn));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call delete once delete btn clicked', () => {
    spyOn(component, 'delete');

    deleteBtn.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(component.delete).toHaveBeenCalled();
  });

  it('should call emit method with ID', () => {
      mockCourse.id = 20;
      spyOn(component.deleteOutput, 'emit');

      deleteBtn.triggerEventHandler('click', null);
      fixture.detectChanges();

      expect(component.deleteOutput.emit).toHaveBeenCalledWith(20);
  });
});

// Test host

describe('CoursesListComponent inside test host', () => {
  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let courseDurationEl: HTMLElement;
  let courseCreationDateEl: HTMLElement;
  let courseTitleEl: HTMLElement;
  let courseDescriptionEl: HTMLElement;
  const datePipe: DatePipe = new DatePipe('en-US');
  const durationPipe: DurationPipe = new DurationPipe();

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CourseComponent, TestHostComponent, DurationPipe]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;
    courseDurationEl = fixture.nativeElement.querySelector(SELECTORS.courseDuration);
    courseCreationDateEl = fixture.nativeElement.querySelector(SELECTORS.courseCreationDate);
    courseTitleEl = fixture.nativeElement.querySelector(SELECTORS.courseTitle);
    courseDescriptionEl = fixture.nativeElement.querySelector(SELECTORS.courseDescription);

    fixture.detectChanges();
  });

  it('should render proper course duration from input' , () => {
    expect(courseDurationEl.textContent).toEqual('2h read');
  });

  it('should render proper course creationDate from input' , () => {
    expect(courseCreationDateEl.textContent).toEqual('Sep 2, 2025');
  });

  it('should render proper course title from input' , () => {
    expect(courseTitleEl.textContent).toEqual(component.coursesArr[0].name);
  });

  it('should render proper course description from input' , () => {
    expect(courseDescriptionEl.textContent).toEqual(component.coursesArr[0].description);
  });
});

// test host component

@Component({
  template: `
    <app-course
      (deleteOutput)="deleteCourse($event)"
      [course]="coursesArr[0]"
    ></app-course>
    `
})

class TestHostComponent{
  coursesArr: Course[] = [
    {
      id: 55,
      date: Date.parse('Sept 02, 25'),
      length: 120,
      description: `Fog`,
      name: 'I',
      isTopRated: true
    },
    {
      id: 55,
      date: Date.parse('Sept 02, 20'),
      length: 5,
      description: `Fog`,
      name: 'I',
      isTopRated: true
    },
  ];
  deleteCourse(id: number): void {
    console.log(id);
  }
}
