import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {Course} from '../../core/models/course.model';

@Component({
  selector: 'app-course',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss']
})


export class CourseComponent {
  @Input() course: Course;
  @Output() deleteOutput: EventEmitter<number> = new EventEmitter<number>();

  delete(): void {
    this.deleteOutput.emit(this.course.id);
  }
}
