import {Component} from '@angular/core';
import {AuthenticationService} from '../../core/services/authentication.service';
import {User} from '../../core/models/user.model';
import {Store} from '@ngrx/store';
import {loadAuths} from '../../store/actions/auth.actions';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent {
  user: User = {
    login: '',
    password: ''
  };

  constructor(private authenticationService: AuthenticationService,
              private store: Store) {
  }

  onLogIn(): void {
    this.store.dispatch(loadAuths());
    this.authenticationService.logIn(this.user);
  }
}
