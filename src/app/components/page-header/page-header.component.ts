import {Component, DoCheck, OnInit} from '@angular/core';
import {AuthenticationService} from '../../core/services/authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent implements DoCheck, OnInit {
  isAuthenticated: boolean;
  userInfo;

  constructor(private authenticationService: AuthenticationService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.authenticationService.userInfoSubj.subscribe(value => this.userInfo = value);

    if (sessionStorage.getItem('userInfo')) {
      this.userInfo = JSON.parse(sessionStorage.getItem('userInfo'));
    }
  }

  ngDoCheck(): void {
    this.authenticationService.isLoggedIn().subscribe(
      val => this.isAuthenticated = val
    );
  }

  logOut(): void {
    this.authenticationService.logOut();
    this.router.navigate(['/login']);
  }
}
