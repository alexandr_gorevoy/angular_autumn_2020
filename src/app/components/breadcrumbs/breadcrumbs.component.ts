import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {CoursesService} from '../../core/services/courses.service';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent implements OnInit{
  breadcrumb: string;
  isCoursesPage: boolean;

  constructor(private courseService: CoursesService,
              private route: ActivatedRoute,
              private router: Router) {}

  ngOnInit(): void {
    this.router.events.subscribe((event) => {

      if (event instanceof NavigationEnd) {
        this.isCoursesPage = !!this.route.snapshot.firstChild.url.length;
        this.breadcrumb = this.createBreadcrumb();
      }

    });
  }



  createBreadcrumb(): string {
    const routeParam = +this.route.snapshot.firstChild.url[0]?.path;
    const course = this.courseService.getItemById(routeParam);

    return course ? course.name : 'New';
  }
}
