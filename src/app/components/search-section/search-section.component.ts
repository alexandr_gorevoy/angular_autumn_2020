import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {debounceTime, filter} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-search-section',
  templateUrl: './search-section.component.html',
  styleUrls: ['./search-section.component.scss']
})
export class SearchSectionComponent implements OnInit {
  searchFieldText: string;
  searchTermSubject = new Subject();

  @Output() searchOutput: EventEmitter<string> = new EventEmitter<string>();

  ngOnInit(): void {
    this.searchTermSubject.pipe(
      debounceTime(500),
      filter((x: string) => x.length >= 3),
    ).subscribe(x => this.searchOutput.emit(x));
  }

  oninputChange(): void {
    this.searchTermSubject.next(this.searchFieldText);
  }
}
