import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchSectionComponent } from './search-section.component';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';

const SELECTORS = {
  searchBtn: `[data-testId="searchBtn"]`
};

describe('SearchSectionComponent', () => {
  let component: SearchSectionComponent;
  let fixture: ComponentFixture<SearchSectionComponent>;
  let searchBtn: DebugElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchSectionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchSectionComponent);
    component = fixture.componentInstance;
    searchBtn = fixture.debugElement.query(By.css(SELECTORS.searchBtn));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call doSearch once read-more__btn clicked', () => {
    spyOn(component, 'doSearch');

    searchBtn.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(component.doSearch).toHaveBeenCalled();
  });

  it('should call emit method with ID', () => {
    component.searchFieldText = 'Search text';
    spyOn(component.searchOutput, 'emit');

    searchBtn.triggerEventHandler('click', null);
    fixture.detectChanges();

    expect(component.searchOutput.emit).toHaveBeenCalledWith('Search text');
  });
});
