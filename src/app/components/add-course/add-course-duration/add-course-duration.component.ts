import {Component, Input} from '@angular/core';
import {Course} from '../../../core/models/course.model';

@Component({
  selector: 'app-add-course-duration',
  templateUrl: './add-course-duration.component.html',
  styleUrls: ['./add-course-duration.component.scss']
})
export class AddCourseDurationComponent{
  @Input() course: Course;
}
