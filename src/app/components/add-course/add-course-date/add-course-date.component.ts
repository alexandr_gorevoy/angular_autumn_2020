import {Component, Input} from '@angular/core';
import {Course} from '../../../core/models/course.model';

@Component({
  selector: 'app-add-course-date',
  templateUrl: './add-course-date.component.html',
  styleUrls: ['./add-course-date.component.scss']
})
export class AddCourseDateComponent {
  @Input() course: Course;
}
