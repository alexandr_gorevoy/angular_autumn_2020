import {Component, OnInit} from '@angular/core';
import {Course} from '../../core/models/course.model';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {CoursesService} from '../../core/services/courses.service';
import {Store} from '@ngrx/store';
import {courseAdded} from '../../store/actions/course-modifyed.actions';
import {throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';

@Component({
  selector: 'app-add-course',
  templateUrl: './add-course.component.html',
  styleUrls: ['./add-course.component.scss']
})
export class AddCourseComponent implements OnInit {

  course: Course;

  constructor(private route: ActivatedRoute,
              private courseService: CoursesService,
              private router: Router,
              private store: Store) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.course = {...this.courseService.getItemById(+params.id)};
      this.course.authors = this.course.authors?.map(author => author.name);
    });
  }

  save(): any {
    this.courseService.createCourse(this.courseService.prepareDataForServer(this.course))
      .pipe(
        tap(data => {
          this.store.dispatch(courseAdded());
          this.router.navigate(['/courses']);
        }),
        catchError(err => {
          console.error(err);
          return throwError(err);
        }),
      ).subscribe();
  }
}
