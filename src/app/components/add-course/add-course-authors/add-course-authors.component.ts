import {Component, Input} from '@angular/core';
import {Course} from '../../../core/models/course.model';

@Component({
  selector: 'app-add-course-authors',
  templateUrl: './add-course-authors.component.html',
  styleUrls: ['./add-course-authors.component.scss']
})
export class AddCourseAuthorsComponent {
  @Input() course: Course;
}
