import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {CoursePageComponent} from './components/course-page/course-page.component';
import {LoginPageComponent} from './components/login-page/login-page.component';
import {AddCourseComponent} from './components/add-course/add-course.component';
import {ErrorPageComponent} from './components/error-page/error-page.component';
import {AuthGuard} from './core/guards/auth.guard';
import {CoursePageContainerComponent} from './components/course-page/course-page-container/course-page-container.component';

const appRoutes: Routes = [
  {path: 'courses', component: CoursePageComponent, canActivate: [AuthGuard],
    children: [
      {path: 'new', component: AddCourseComponent},
      {path: ':id', component: AddCourseComponent},
      {path: '', component: CoursePageContainerComponent},
    ]},
  {path: 'login', component: LoginPageComponent},
  {path: 'error', component: ErrorPageComponent},
  {path: '', redirectTo: '/courses', pathMatch: 'full', canActivate: [AuthGuard]},
  {path: '**', redirectTo: '/error'}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
