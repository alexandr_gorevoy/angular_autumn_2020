export interface Course {
  name: string;
  description: string;
  date: number;
  length: number;
  authors?: Array<any>;
  id: number;
  isTopRated: boolean;
}
