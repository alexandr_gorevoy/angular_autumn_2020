import {Injectable} from '@angular/core';
import {Course} from '../models/course.model';
import {HttpService} from './http.service';
import {Observable} from 'rxjs';
import {loadingCoursesInProgress} from '../../store/actions/courses.actions';
import {Store} from '@ngrx/store';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {
  coursesFromServer = [];

  constructor(private http: HttpService,
              private store: Store) {
  }

  getCoursesList(): Observable<Course[]> {
    this.store.dispatch(loadingCoursesInProgress());
    return this.http.getCoursesList();
  }

  createCourse(course): Observable<Course> {
    return this.http.createNewCourse(course);
  }

  getItemById(id: number): Course {
    return this.coursesFromServer?.find((item) => id === item.id) || {};
  }

  updateItem(): void {
    console.log('Item updater');
  }

  removeItem(id: number): Observable<any> {
    return this.http.deleteCourse(id);
  }

  prepareCourseAuthors(authors): object {
    return authors.split(',').map(author => {
      return {
        name: author,
        id: 1
      };
    });
  }

  generateId(): number {
    return Math.floor(Math.random() * Date.now());
  }

  prepareDataForServer(course): Course {
    course.isTopRated = true;
    course.id = this.generateId();
    course.date = Date.parse(String(course.date));
    course.authors = this.prepareCourseAuthors(course.authors);

    return course;
  }
}
