import {Injectable} from '@angular/core';

import {HttpService} from './http.service';
import {Router} from '@angular/router';
import {Observable, of, Subject, throwError} from 'rxjs';
import {catchError, concatMap, take, tap} from 'rxjs/operators';
import {select, Store} from '@ngrx/store';
import {loadAuthsFailure, loadAuthsLoggedOff, loadAuthsSuccess} from '../../store/actions/auth.actions';
import {selectAuth} from '../../store/selectors/auth.selectors';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  userInfoSubj = new Subject();

  constructor(private httpService: HttpService,
              private router: Router,
              private store: Store) {
  }

  logIn(user): any {
    this.httpService.getAuthToker(user).pipe(
      tap(
        response => {
          sessionStorage.setItem('isAuthenticated', 'true');
          sessionStorage.setItem('token', response.token.toString());

          this.store.dispatch(loadAuthsSuccess({user, isAuthenticated: true, errorMessage: null, token: response.token}));
          concatMap(this.setUserNameToStorage());
        }
      ),
      catchError(err => {
        this.store.dispatch(loadAuthsFailure({user: null, isAuthenticated: false, errorMessage: err.message}));
        return err;
      })
    ).subscribe();
  }

  logOut(): void {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('userInfo');
    sessionStorage.setItem('isAuthenticated', 'false');
    this.store.dispatch(loadAuthsLoggedOff());
  }

  isLoggedIn(): Observable<boolean> {
    return of(sessionStorage.getItem('isAuthenticated') === 'true');
  }

  getUserInfo(token): Observable<any> {
    return this.httpService.getUserInfo(token);
  }

  getTokenFromSessionStorage(): string {
    return sessionStorage.getItem('token');
  }

  setUserNameToStorage(): any {
    this.store.pipe(
      take(1),
      select(selectAuth),
      tap(data => {
        this.getUserInfo(data.token)
          .subscribe(value => {
            sessionStorage.setItem('userInfo', JSON.stringify(value.name));
            this.userInfoSubj.next(value.name);

            this.router.navigate(['/courses']);
          });
      }),
      catchError(err => {
        console.error(err);
        return throwError(err);
      })
    ).subscribe();
  }
}
