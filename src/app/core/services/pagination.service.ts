import { Injectable } from '@angular/core';
import {Course} from '../models/course.model';

@Injectable({
  providedIn: 'root'
})
export class PaginationService {
  coursesOnPage = 10;
  pagesAmount = [];
  currentPage = 1;

  constructor() { }

  setPagesAmount(coursesArr): void {
     this.pagesAmount.length = !this.pagesAmount.length ?
      Math.ceil(coursesArr.length / this.coursesOnPage) :
      this.pagesAmount.length;
  }

  getCoursesforSpecificPage(pageNumber, allCourses): Course[] {
    const maxCourseIndexOnPage = pageNumber * this.coursesOnPage;
    const minCourseIndexOnPage = (pageNumber - 1) * this.coursesOnPage;

    return allCourses.filter((course) => {
      const indexOfCourse = allCourses.indexOf(course);

      if (indexOfCourse < maxCourseIndexOnPage && indexOfCourse >= minCourseIndexOnPage) {
        return course;
      }
    });
  }
}
