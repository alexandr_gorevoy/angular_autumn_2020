import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

const BASE_URL = 'http://localhost:3004';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  constructor(private http: HttpClient) { }

  getAuthToker(userData): Observable<any> {
    return this.http.post(`${BASE_URL}/auth/login`, userData);
  }

  getUserInfo(token): Observable<object> {
    return this.http.post(`${BASE_URL}/auth/userinfo`, {token});
  }

  getCoursesList(): Observable<any> {
    return this.http.get(`${BASE_URL}/courses?sort=date`);
  }

  getFilteredCoursesList(filteredString): Observable<any> {
    return this.http.get(`${BASE_URL}/courses?sort=date&textFragment=${filteredString}`);
  }

  deleteCourse(id: number): Observable<any> {
    return this.http.delete(`${BASE_URL}/courses/${id}`);
  }

  createNewCourse(course): Observable<any> {
    return this.http.post(`${BASE_URL}/courses`, course);
  }
}
