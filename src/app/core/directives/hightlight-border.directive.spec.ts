import { HightlightBorderDirective } from './hightlight-border.directive';
import {Component, ElementRef, Renderer2} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {Course} from '../models/course.model';

describe('HightlightBorderDirective', () => {
  it('should create an instance', () => {
    let el: ElementRef;
    let r: Renderer2;
    const directive = new HightlightBorderDirective(el, r);
    expect(directive).toBeTruthy();
  });
});

// Test host

const SELECTORS = {
  coursesWrapper: `[data-testId="coursesWrapper"]`
};

describe('CoursesListComponent inside test host', () => {
  let component: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let coursesElements: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestHostComponent, HightlightBorderDirective]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    coursesElements = fixture.nativeElement.querySelectorAll(SELECTORS.coursesWrapper);
  });

  it('should set border to blue if course.creationDate > currentDate' , () => {
    expect(coursesElements[0].style.border).toEqual('3px solid blue');
  });

  it('should set border to green if course.creationDate < currentDate and course was created < 14 days ago' , () => {
    expect(coursesElements[1].style.border).toEqual('3px solid green');
  });

  it('should not set border if course was created > 14 days ago' , () => {
    expect(coursesElements[2].style.border).toEqual('');
  });
});

// test host component

@Component({
  template: `
    <div [appHightlightBorder]="course.creationDate"
         *ngFor="let course of coursesArr"
         data-testId="coursesWrapper">
      <p>{{course.creationDate}}</p>
    </div>
    `
})

class TestHostComponent{
  millisecondsInDay = 86400000;

  coursesArr: Course[] =  [
    {
      id: 1,
      creationDate: Date.now() + this.millisecondsInDay,
      duration: 128,
      description: `again.`,
      title: 'In the Future We Will All Live in Space',
      topRated: false
    },
    {
      id: 1,
      creationDate: Date.now() - this.millisecondsInDay,
      duration: 60,
      description: `Fog`,
      title: 'In the Future We Will All Live in Hell',
      topRated: true
    },
    {
      id: 1,
      creationDate: Date.parse('Oct 02, 20'),
      duration: 12,
      description: `bla`,
      title: 'London is the capital of Great Britain',
      topRated: true
    }
  ];
}
