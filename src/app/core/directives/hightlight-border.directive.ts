import {AfterContentInit, Directive, ElementRef, Input, Renderer2} from '@angular/core';

@Directive({
  selector: '[appHightlightBorder]'
})
export class HightlightBorderDirective implements  AfterContentInit{
  @Input('appHightlightBorder') courseCreationDate: number;

  constructor(private el: ElementRef, private r: Renderer2) {}

  ngAfterContentInit(): void {
    const daysToCheck = 1000 * 3600 * 24 * 14;
    const currentDate = Date.now();
    const timeDifference = currentDate - this.courseCreationDate;

    if (currentDate > this.courseCreationDate && timeDifference <= daysToCheck) {
      this.r.setStyle(this.el.nativeElement, 'border', '3px solid green');
    } else if (this.courseCreationDate > currentDate) {
      this.r.setStyle(this.el.nativeElement, 'border', '3px solid blue');
    }
  }
}
