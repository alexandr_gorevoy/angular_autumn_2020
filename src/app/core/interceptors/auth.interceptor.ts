import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {AuthenticationService} from '../services/authentication.service';
import {finalize} from 'rxjs/operators';
import {LoaderService} from '../services/loader.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor{
  constructor(private authService: AuthenticationService,
              public loaderService: LoaderService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.loaderService.show();
    const clone = req.clone({
       headers: req.headers.append('Auth', String(this.authService.getTokenFromSessionStorage()))
    });

    return next.handle(clone)
      .pipe(
        finalize(() => this.loaderService.hide())
      );
  }
}
