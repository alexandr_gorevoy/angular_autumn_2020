// import { FilterPipe } from './filter.pipe';
// import {Course} from '../models/course.model';
//
// describe('FilterPipe', () => {
//   const filterPipe = new FilterPipe();
//   const millisecondsInDay = 86400000;
//   let courses: Course[];
//
//   beforeEach(() => {
//     courses = [
//       {
//         id: 1,
//         creationDate: Date.now() + millisecondsInDay,
//         duration: 128,
//         description: `Fog down the river`,
//         title: 'In the Future We Will All Live in Space',
//         topRated: false
//       },
//       {
//         id: 1,
//         creationDate: Date.parse('Sept 02, 20'),
//         duration: 132,
//         description: `Fog down the river`,
//         title: 'In the Future We Will All Live in Heaven',
//         topRated: true
//       },
//       {
//         id: 1,
//         creationDate: Date.now() - millisecondsInDay,
//         duration: 60,
//         description: `Fog down the river`,
//         title: 'In the Future We Will All Live in Hell',
//         topRated: true
//       },
//       {
//         id: 1,
//         creationDate: Date.parse('Oct 02, 20'),
//         duration: 12,
//         description: `Fog down the river`,
//         title: 'London is the capital of Great Britain',
//         topRated: true
//       }
//     ];
//   });
//
//   it('create an instance', () => {
//     expect(filterPipe).toBeTruthy();
//   });
//
//   it('should filter courses array and left only one instance', () => {
//     courses = filterPipe.transform(courses, 'Heaven');
//
//     expect(courses.length).toEqual(1);
//   });
//
//   it('should filtered item title to be London is the capital of Great Britain', () => {
//     courses = filterPipe.transform(courses, 'Great');
//
//     expect(courses[0].title).toEqual('London is the capital of Great Britain');
//   });
// });
