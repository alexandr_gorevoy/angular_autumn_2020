// import { OrderByPipe } from './order-by.pipe';
// import {Course} from '../models/course.model';
//
// describe('OrderBy', () => {
//   const orderBy = new OrderByPipe();
//   let courses: Course[];
//
//   beforeEach(() => {
//     courses = [
//       {
//         id: 1,
//         creationDate: 3,
//         duration: 128,
//         description: `Fog down the river`,
//         title: 'In the Future We Will All Live in Space',
//         topRated: false
//       },
//       {
//         id: 1,
//         creationDate: 1,
//         duration: 132,
//         description: `Fog down the river`,
//         title: 'In the Future We Will All Live in Heaven',
//         topRated: true
//       },
//       {
//         id: 1,
//         creationDate: 55,
//         duration: 60,
//         description: `Fog down the river`,
//         title: 'In the Future We Will All Live in Hell',
//         topRated: true
//       },
//       {
//         id: 1,
//         creationDate: 4,
//         duration: 12,
//         description: `Fog down the river`,
//         title: 'London is the capital of Great Britain',
//         topRated: true
//       }
//     ];
//   });
//
//   it('create an instance', () => {
//     expect(orderBy).toBeTruthy();
//   });
//
//   it('should first element of courses array be with the highest creationDate field', () => {
//     courses.push({description: '', duration: 0, id: 0, title: '', topRated: false, creationDate: 888});
//     courses = orderBy.transform(courses);
//
//     expect(courses[0].creationDate).toEqual(888);
//   });
//
//   it('should last element of courses array be with the lowest creationDate field', () => {
//     courses.unshift({description: '', duration: 0, id: 0, title: '', topRated: false, creationDate: 0});
//     courses = orderBy.transform(courses);
//
//     expect(courses[courses.length - 1].creationDate).toEqual(0);
//   });
// });
