// Replaced with server request
// import { Pipe, PipeTransform } from '@angular/core';
// import {Course} from '../models/course.model';
//
// @Pipe({
//   name: 'filter',
// })
// export class FilterPipe implements PipeTransform {
//   transform(courses: Course[], searchString: string = ''): Course[]{
//     if (typeof courses !== 'object'){
//       console.error('Wrong argument for courses in FilterPipe. An Object is expected');
//       return courses;
//     }
//
//     if (typeof searchString !== 'string') {
//       console.error('Wrong argument for searchString in FilterPipe. A String is expected');
//       return searchString;
//     }
//
//     return courses.filter((item) => item.name.toLowerCase().includes(searchString.toLowerCase()));
//   }
// }
