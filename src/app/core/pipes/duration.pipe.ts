import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'duration'
})
export class DurationPipe implements PipeTransform {

  transform(minutes: number): string {
    if (typeof minutes !== 'number'){
      console.error('Wrong argument for DurationPipe. A Number is expected');
      return minutes;
    }

    const hour = 60;

    if (minutes < hour) {
      return `${minutes}min`;
    } else {
      const hours = Math.floor(minutes / hour);
      const minutesLeft = minutes % hour > 0 ? `, ${minutes % hour}min` : '';

      return `${hours}h${minutesLeft}`;
    }
  }
}
