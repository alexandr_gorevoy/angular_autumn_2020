// Replaced with server request
// import { Pipe, PipeTransform } from '@angular/core';
// import {Course} from '../models/course.model';
//
// @Pipe({
//   name: 'orderBy'
// })
// export class OrderByPipe implements PipeTransform {
//
//   transform(courses): Course[]{
//     courses.forEach(course => course.date = Date.parse(course.date));
//
//     return courses.sort((a, b) => {
//       return b.date - a.date;
//     });
//   }
// }
