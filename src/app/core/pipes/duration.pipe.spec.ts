import {DurationPipe} from './duration.pipe';

describe('DurationPipe', () => {
  const durationPipe = new DurationPipe();
  let transformedValue;

  it('create an instance', () => {
    expect(durationPipe).toBeTruthy();
  });

  it('should transform value 12 to 12min' , () => {
    transformedValue = durationPipe.transform(12);

    expect(transformedValue).toEqual('12min');
  });

  it('should transform value 112 to 1h 52min' , () => {
    transformedValue = durationPipe.transform(112);

    expect(transformedValue).toEqual('1h, 52min');
  });

  it('should transform value 60 to 1h' , () => {
    transformedValue = durationPipe.transform(60);

    expect(transformedValue).toEqual('1h');
  });
});
